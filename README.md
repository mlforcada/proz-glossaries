# proz-glossaries

Very experimental bash shell to download some ProZ glossaries into tab-separated OmegaT glossaries.

First, go to [https://www.proz.com/search/], select your source and target language and look for a glossary containing a specific term.

Among the listed results, look for those having just two languages (for instance *German to Spanish*) where *Source* is a *Glossary*.

Visit the glossary (a couple of clicks away) and then copy the URL of the link to Page 1.

Links should look like this one: 
```
https://www.proz.com/personal-glossaries/18889-software?page=1
``` 

Then launch the shell:
``` 
./download-proz-glossary.sh https://www.proz.com/personal-glossaries/18889-software?page=1
``` 

When it's done downloading and extracting, you'll find a file such as
```
18889-software.txt
```

This is your OmegaT glossary: just drop it in the *glossary* folder *et voilà*.

## To do

* Some glossaries contain comments, they should be added as a third field.
* More extensive testing.
* Dealing with multilingual glossaries.

## Notes on how to make it work for Windows (still have to try: let me know if you make it work)

* Install Git [https://git-scm.com/download/win] and you'll get the oh-so-cool Git Bash shell.
* Installing wget, following this advice [https://gist.github.com/evanwill/0207876c3243bbb6863e65ec5dc3f058]
* Rewriting the shell to use the $RANDOM variable instead of mktemp (or installing it from [http://gnuwin32.sourceforge.net/packages/mktemp.htm])



