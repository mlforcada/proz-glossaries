# (c) Mikel L. Forcada 2019
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# Turns ProZ uploaded glossaries (some of them) into OmegaT tab-separated .txt files
# Still experimental:
# Only bilingual 
# Only glossaries --- not glosspost

# will read from command line
URL=$1

# the page 1 (or any page) link works. Other links don't
if [[ ! $URL =~ page=[0-9]+ ]]; then
  echo "Aborting"
  exit 
fi 

# strip number from URL read from command line 
NURL=$(echo $URL | sed 's/page=[0-9]\+/page=/g')
# echo $NURL

# get filename for result 
RESULT=$(echo $NURL | sed 's/^.*[/]\(.*\)[?].*$/\1/g')

# Get number of pages from any of them (hack)
TMP=$(mktemp -d -t aux-XXXXXXXXXX)
cd $TMP
# echo $TMP
wget -nv $URL 
NUMBEROFPAGES=$(cat * | sed 's/</\n</g' | grep "page=[0-9]\+\">[0-9]\+" | tail -1 | sed 's/<[^>]\+>//g')
cd -
rm -rf $TMP
echo "Number of pages found=" $NUMBEROFPAGES


#create temporary directory
TMP=$(mktemp -d -t proz-XXXXXXXXXX)

cd $TMP

for i in $(seq 1 $NUMBEROFPAGES) 
do 
  wget -nv $NURL$i;
done
echo "All pages downloaded"

# for i in {1..$NUMBEROFPAGES}; do  done

# Hack around to extract
cat * | sed 's/<span class=.gloss-term gloss-term-source.>/\nSOURCE:/g; s/<span class=.gloss-term gloss-term-target.>/\nTARGET:/g' | egrep "(SOURCE:|TARGET:)" | sed 's/<\/a>.*$//g' | sed 's/<[^>]\+>//g' | sed 's/TARGET:/@/g' | sed 's/SOURCE://g' | sed -z 's/\n@/\t/g' >$RESULT

echo "Result file created"

cd -

cat $TMP/$RESULT | sort > $RESULT".txt"
rm -rf $TMP


